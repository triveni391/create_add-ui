import constants from "./constants";
import { get } from "../service";

function getDomainAction() {
  return async dispatch => {
    dispatch({ type: constants.GET_DOMAIN_LOADING });
    try {
      let res = await get('/domain');
      if (res.error) {
        dispatch({ type: constants.GET_DOMAIN_FAILURE, message: res.message });
      } else
        dispatch({ type: constants.GET_DOMAIN_SUCCESS, data: res.data })
    } catch (ex) {
      console.error('error in getting user', ex);
      dispatch({ type: constants.GET_DOMAIN_FAILURE, message: ex.message });
    }
  }
}


export {
  getDomainAction
}