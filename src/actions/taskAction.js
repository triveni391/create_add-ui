import constants from "./constants";
import { createTask as createTaskAction, get } from "../service"

function createTask(task) {
  return async dispatch => {
    dispatch({ type: constants.CREATE_TASK_LOADING });
    try {
      let res = await createTaskAction(task);
      if (res.error) {
        dispatch({ type: constants.CREATE_TASK_FAILURE, message: res.message });
      } else {
        dispatch({ type: constants.CREATE_TASK_SUCCESS });
      }
    } catch (ex) {
      console.error('error in creating task', ex)
      dispatch({ type: constants.CREATE_TASK_FAILURE, message: ex.message });
    }
  }
}


function getTaskList() {
  return async dispatch => {
    dispatch({ type: constants.GET_TASK_LOADING });
    try {
      const res = await get('/list');
      dispatch({ type: constants.GET_TASK_SUCCESS, data: res.data.tasks })
    } catch (ex) {
      console.error('error in getting tasks', ex);
      dispatch({ type: constants.GET_TASK_FAILURE, message: ex.message });
    }
  }

}

export {
  createTask, getTaskList
}

