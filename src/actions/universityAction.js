import constants from "./constants";
import { get, getUrl } from "../service";

function getUniversityAction(queryParams = null) {
  return async dispatch => {
    dispatch({ type: constants.GET_UNIVERSITY_LOADING });
    let url = '/universities';
    if (Object.keys(queryParams).length) {
      url += await getUrl(queryParams);
    }
    try {
      const res = await get(url);
      if (res.error) {
        dispatch({ type: constants.GET_UNIVERSITY_FAILURE, message: res.message });
      } else
        dispatch({ type: constants.GET_UNIVERSITY_SUCCESS, data: res.data })
    } catch (ex) {
      console.error('error in getting UNIVERSITY', ex);
      dispatch({ type: constants.GET_UNIVERSITY_FAILURE, message: ex.message });
    }
  }
}


export {
  getUniversityAction
}