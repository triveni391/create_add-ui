import constants from "./constants";
import { get } from "../service";

function getCountry() {
  return async dispatch => {
    dispatch({ type: constants.GET_COUNTRY_LOADING });
    try {
      let res = await get('/countrycode');
      if (res.error) {
        dispatch({ type: constants.GET_COUNTRY_FAILURE, message: res.message });
      } else
        dispatch({ type: constants.GET_COUNTRY_SUCCESS, data: res.data })
    } catch (ex) {
      console.error('error in getting user', ex);
      dispatch({ type: constants.GET_COUNTRY_FAILURE, message: ex.message });
    }
  }
}


export {
  getCountry
}