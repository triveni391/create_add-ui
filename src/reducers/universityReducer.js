import constants from "../actions/constants";

export default (state = {
  data: [],
  loading: false,
  error: false
}, action = {}) => {
  switch (action.type) {
    case constants.GET_UNIVERSITY_LOADING: {
      return { ...state, loading: true, error: false }
    }
    case constants.GET_UNIVERSITY_SUCCESS: {
      return { ...state, loading: false, error: false, data: action.data }
    }
    case constants.GET_UNIVERSITY_FAILURE: {
      return { ...state, loading: false, error: true }
    }
  }
  return state;
}