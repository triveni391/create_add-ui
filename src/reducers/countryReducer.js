import constants from "../actions/constants";

export default (state = {
  loading: false,
  error: false,
  data: [],
}, action) => {
  switch (action.type) {
    case constants.GET_COUNTRY_LOADING: {
      return {
        ...state, loading: true, error: false
      }
    }
    case constants.GET_COUNTRY_SUCCESS: {
      return {
        ...state, data: action.data, loading: false
      }
    }
    case constants.GET_COUNTRY_FAILURE: {
      return {
        ...state, loading: false, error: false
      }
    }
    default: { return state }
  }
}