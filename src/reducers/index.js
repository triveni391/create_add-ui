import countries from './countryReducer';
import domains from './domainReducer';
import universities from './universityReducer';
import { combineReducers } from 'redux';

export default combineReducers({
  universities,
  countries,
  domains
});