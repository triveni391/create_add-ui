import constants from "../actions/constants";

export default (state = {
  loading: false,
  error: false,
  data: [],
}, action) => {
  switch (action.type) {
    case constants.GET_DOMAIN_LOADING: {
      return {
        ...state, loading: true, error: false
      }
    }
    case constants.GET_DOMAIN_SUCCESS: {
      return {
        ...state, data: action.data, loading: false
      }
    }
    case constants.GET_DOMAIN_FAILURE: {
      return {
        ...state, loading: false, error: false
      }
    }
    default: { return state }
  }
}