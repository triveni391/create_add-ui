import './App.css';
import Cards from './components/card';
import { Fragment, useState } from 'react';
import { connect, useDispatch } from 'react-redux';
import { getUniversityAction } from './actions/universityAction';
import { useEffect } from 'react';
import styled from 'styled-components';
import { getCountry } from './actions/countryAction';
import { getDomainAction } from './actions/domainAction';
import { media } from './breakpoints';
import isMobile from './utils';
import NoError from './components/NoErrror';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";


const Container = styled.div`
  position: relative;
  margin:  4rem;
`

const Input = styled.input`
  width: ${props => props.width};
  box-shadow: 0 2px 5px 1px rgb(64 60 67 / 16%);
  border-radius: 20px;
  padding: 0.5rem 1rem;
  border: none;
  height: 30px;
  &:focus-visible {
    outline: none;
  }
  ${media.mobileOnly} {
      width: 75%;
  }
`

const Select = styled.select`
  margin-left: 2rem;
  width: ${props => props.width};
  box-shadow: 0 2px 5px 1px rgb(64 60 67 / 16%);
  border-radius: 10px;
  padding: 0.5rem 1rem;
  margin-top: 1rem;

  border: none;
      width: 150px;
    height: 44px;
  &:focus-visible {
    outline: none;
  }
  optgroup: {
    height: 20px
  }
  &:first-child {
    margin-left: 0rem;
  }
`

const FilterContainer = styled.div`
  
  ${media.mobileOnly} {
    position: relative;
    width: 100%;
  display: flex;
  align-items: justify-content;
    flex-direction: column;
    justify-content: space-between;
    margin-left: -2rem;
  }
`

const OptionGrp = styled.optgroup`
   height: 20rem
`

const ClearButton = styled.button`
    background: rgba(0,118,255,0.9);
    color: white;
    border: none;
    width: 100px;
    border-radius: 20px;
    text-transform: capitalize;
    height: 44px;
    margin-left: 1rem;
    ${media.mobileOnly} {
      position: absolute;
      top: 75%;
      right: -4rem;
    }
`

const PaginationContainer = styled.div`
    position: absolute;
    top: 2rem;
    right: 10rem;
    ${media.mobileOnly} {
      display: none
    }
`

const LoadingContainer = styled.div`
    width: 100%;
    height: 500px;
    display: flex;
    justify-content: center;
    align-items: center;
`


function App({ universities, countries, domains, loading }) {
  const defaultValue = Object.freeze({
    page_size: 10,
    page_num: 1
  })
  const dispatch = useDispatch();
  const [filterOptions, setFilterOptions] = useState({ ...defaultValue });
  const mobile = isMobile()


  useEffect(() => {
    if (!countries.length)
      dispatch(getCountry())
    if (!domains.length)
      dispatch(getDomainAction())
  }, [countries, domains]);

  const onChange = async (property, value) => {
    await setFilterOptions({ ...filterOptions, [property]: value })
  }

  useEffect(() => {
    dispatch(getUniversityAction(filterOptions))
  }, [filterOptions]);

  const renderCards = () => {
    if (loading) return <LoadingContainer><Loader color="rgba(0,118,255,0.9)" /></LoadingContainer>
    if (!universities.length) return <NoError msg="No data found" />
    return <Cards items={universities} />
  }

  return <Fragment>
    <Container>
      <Input placeholder="search..." width="400px" onChange={(e) => onChange("search", e.target.value)} />
      <FilterContainer>
        <Select defaultValue="India" onChange={(e) => onChange("alpha_two_code", e.target.value)}>
          <OptionGrp label="country">
            {countries.map(item => <option value={item.code}>{item.country}</option>)}
          </OptionGrp>
        </Select>
        <Select defaultValue="India" onChange={(e) => onChange("domain", e.target.value)}>
          <OptionGrp label="domain">
            {domains.map(item => <option value={item.domain}>{item.domain}</option>)}
          </OptionGrp>
        </Select>
        {mobile && <Select onChange={(e) => onChange("page_num", e.target.value)}>
          <OptionGrp label="pagenumber" >
            {[1, 2, 4, 5].map(item => <option value={item}>{item}</option>)}
          </OptionGrp>
        </Select>}
        <ClearButton onClick={() => setFilterOptions({ ...defaultValue })}>clear filter</ClearButton>
      </FilterContainer>
      {renderCards()}
      <PaginationContainer>
        <label>Page Number</label>
        <Select onChange={(e) => onChange("page_num", e.target.value)}>
          <OptionGrp label="pagenumber">
            {[1, 2, 4, 5].map(item => <option value={item}>{item}</option>)}
          </OptionGrp>
        </Select>
      </PaginationContainer>
    </Container></Fragment>
}

function mapStateToProps(store) {
  return {
    universities: store.universities.data,
    loading: store.universities.loading,
    countries: store.countries.data,
    domains: store.domains.data
  }
}

export default connect(mapStateToProps)(App);
