import React from "react";
import styled from "styled-components";

const P = styled.p`
  color: red;
  font-weight: 600;
  width: 100%;
  align-items: center;
  justify-content: center;
  height: 200px;
  display: flex;
`
const NoError = ({ msg }) => {
  return <P>{msg}</P>
}

export default NoError;