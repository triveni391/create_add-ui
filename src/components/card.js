import styled from "styled-components"
import { media } from "../breakpoints"
import uni from "../assets/uni.jpeg"

const CardContainer = styled.div`
    display: grid;
    grid-gap: 1rem;
    margin: 4rem auto;
    grid-template-columns: 33% 33% 33%;
    ${media.mobileOnly} {
      margin-right: 0;
      margin-left: 0;
      width:250px;
      grid-template-columns: 100%;
    }
`
function Cards({ items = [] }) {
  return <CardContainer>
    {items.map(item => <Card item={item} />)}
  </CardContainer>
}

const Box = styled.div`
    width: 90%;
    height: 200px;
    padding: 1rem;
    border-radius: 5px;
    padding: 1rem;
    display: flex;
    border: 1px solid #eaeaea;
    box-shadow: 0 2px 6px rgb(0 0 0 / 15%);
    -webkit-transition: box-shadow 0.2s ease;
    transition: box-shadow 0.2s ease;
    ${media.mobileOnly} {
      flex-direction: column;
      height: auto;
    }
`

const Header = styled.h4`
  font-size: 1rem;
`

const Img = styled.img`
    width: 200px;
    height: 200px;
    border-radius: 50%;
    ${media.mobileOnly} {
      width: 100%;
    }
`
const ImageContainer = styled.div`
    ${media.mobileOnly} {
      width: 100%
    }
`
const ContentContainer = styled.div`
    margin-left: 1rem;
`

const A = styled.a`
    text-decoration: none;
    color: #0074de;
    word-break: break-word;
          font-weight: 600;
    &:hover {
      font-weight: 200;
    }
`

function Card({ item }) {
  return <Box>
    <ImageContainer>
      <Img src={uni} />
    </ImageContainer>
    <ContentContainer>
      <Header>{item.name}</Header>
      <A href={item.web_pages}>{item.web_pages}</A>
    </ContentContainer>

  </Box>
}

export default Cards