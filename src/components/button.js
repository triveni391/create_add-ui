import styled from "styled-components";

const Button = styled.button`
    height: 3rem;
    background: ${props => props.type === 'submit' ? 'rgba(0,118,255,0.9)' : 'inherit'};
    box-shadow: 0 2px 6px rgb(0 0 0 / 15%);
    transition: box-shadow 0.2s ease;
    border-radius: 1rem;
    border: 1px solid #eaeaea;
    pointer: cursor;
    font-size: 1em;
    padding: 0.25rem 1rem;
    margin:20px auto;
`

export default function ButtonComponent({ children, onClick, type }) {
    return <Button onClick={onClick} type={type}>{children}</Button>
}