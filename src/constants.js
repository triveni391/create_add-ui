const constants = {
    PRIORITY: {
        HIGH: 3,
        MEDIUM: 2,
        LOW: 1
    }
}

export default constants;