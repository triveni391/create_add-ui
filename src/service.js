import axios from 'axios';
import config from './config.json';

const instance = axios.create({
  baseURL: config.server
});

async function get(url) {
  try {
    return await instance.get(url);
  } catch (ex) {
    console.error('error in getting task');
    throw ex;
  }
}


export const getUrl = (fields) => {
  let url = '?';
  for (let key in fields) {
    if (Object.hasOwnProperty.call(fields, key) && fields[key]) {
      url += `${key}=${fields[key]}&`
    }
  }
  return url;
}
export { get }